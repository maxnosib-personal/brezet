#!/bin/sh
# запоминаем где мы находимся
export start_folder=$(pwd)
# стягиваем мигратор
go get github.com/ne-ray/migrago
# собираем мигратор -o указывает название бинарника
cd $GOPATH/src/github.com/ne-ray/migrago && go build -o migrago && cp migrago $GOBIN
# возвращяемся в домашнюю папку
cd $start_folder
# создаем папку data для сохранения состояния миграций
mkdir data
# создаем сеть на всякий случай
docker network create net

# устанавливаем линтер golangci-lint
curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s v1.23.6