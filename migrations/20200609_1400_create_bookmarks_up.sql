CREATE TABLE IF NOT EXISTS bookmarks (
    id serial NOT NULL PRIMARY KEY,
    url varchar(255) NOT NULL,
    category_id int NOT NULL  DEFAULT 1,
    title varchar(255) NOT NULL,
    description varchar(255) NOT NULL,
    image varchar(255) NOT NULL,
    hashtag_ids int[] NOT NULL DEFAULT ARRAY[]::integer[],
    user_id int NOT NULL,
    created_at int NOT NULL,
    updated_at int NOT NULL,
    CONSTRAINT bookmark_category_fk FOREIGN KEY (category_id) REFERENCES categories(id) ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT bookmark_user_fk FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT bookmark_unq UNIQUE(url,user_id)
);
