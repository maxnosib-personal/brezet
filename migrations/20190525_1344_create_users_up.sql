CREATE TABLE IF NOT EXISTS users (
    id serial NOT NULL PRIMARY KEY,
    login varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    CONSTRAINT user_unq UNIQUE(login)
);