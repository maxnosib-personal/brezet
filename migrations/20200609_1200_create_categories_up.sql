CREATE TABLE IF NOT EXISTS categories (
    id serial NOT NULL PRIMARY KEY,
    name varchar(255) NOT NULL,
    icon varchar(255) NOT NULL,
    parent_id int NULL,
    user_id int NOT NULL,
    created_at int NOT NULL,
    updated_at int NOT NULL,
    CONSTRAINT category_parent_fk FOREIGN KEY (parent_id) REFERENCES categories(id) ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT category_user_fk FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT category_unq UNIQUE(name,user_id)
);
