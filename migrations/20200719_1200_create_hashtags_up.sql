CREATE TABLE IF NOT EXISTS hashtags (
    id serial NOT NULL PRIMARY KEY,
    name varchar(255) NOT NULL,
    CONSTRAINT hashtags_unq UNIQUE(name)
);
