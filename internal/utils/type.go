package utils

import (
	"time"
)

/*********************************** cont ***********************************/

// Type config file
const (
	FileTypeYaml = "yaml"
	FileTypeYml  = "yml"
	FileTypeJSON = "json"
)

// general structures
type (
	// Config structure in which parsing a config
	Config struct {
		App struct {
			Port                 string        `yaml:"port"`
			JwtRefreshExpireDate time.Duration `yaml:"jwtRefreshExpireDate"`
			JwtAccessExpireDate  time.Duration `yaml:"jwtAccessExpireDate"`
			JWTKey               string        `yaml:"JWTKey"`
		} `yaml:"app"`

		Connections struct {
			Pgsql struct {
				Host     string `yaml:"host"`
				Port     int    `yaml:"port"`
				User     string `yaml:"user"`
				Password string `yaml:"password"`
				DBname   string `yaml:"dbname"`
			} `yaml:"pgsql"`
		} `yaml:"connections"`
	}

	// Answer single answer from api
	Answer struct {
		Err  error
		Data interface{}
	}
)
