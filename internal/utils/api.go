package utils

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/securecookie"
)

// создаем LS для хранения токенов авторизации
// чтоб при смене токена нельзя было им поспользоваться
var ls = make(map[string]struct{}) //nolint: gochecknoglobals,gocritic

// ReadJSON json to struct
func ReadJSON(w http.ResponseWriter, r *http.Request, data interface{}) {
	bData, err := ioutil.ReadAll(r.Body)
	if err == nil {
		err = json.Unmarshal(bData, &data)
	}
	if err != nil {
		fmt.Printf("err ReadJSON: %+v\n", err)
		http.Error(w, err.Error(), 400)
	}
}

// WriteJSON struct to json
func WriteJSON(w http.ResponseWriter, res Answer) {
	if res.Err != nil {
		fmt.Printf("err request: %+v\n", res.Err)
		http.Error(w, res.Err.Error(), 400)
		return
	}

	bData, err := json.Marshal(res.Data)
	if err == nil {
		_, err = w.Write(bData)
	}
	if err != nil {
		fmt.Printf("err WriteJSON: %+v\n", err)
		http.Error(w, res.Err.Error(), 400)
	}
}

// ReadCookieHandler чтение и проверка кук
func ReadCookieHandler(w http.ResponseWriter, r *http.Request) (map[int]string, error) {
	// INFO: размерность ключа 16,24,32
	var hashKey = []byte("0Lh|v|8WKCXapCpJQbTn}~Nb4o1j{zNC") // TODO: вынести в конфиг этот ключ
	var blockKey = []byte("F}arxw1nJJyPqFG%s3~u*YlUfNZQ@G85")
	var s = securecookie.New(hashKey, blockKey)
	cookie, err := r.Cookie("session")
	if err != nil {
		return nil, err
	}
	// проверяем есть ли токен пользователя
	if _, ok := ls[cookie.Value]; !ok {
		return nil, errors.New("user is not authorized")
	}

	value := make(map[int]string)
	if err := s.Decode("session", cookie.Value, &value); err != nil {
		return nil, err
	}
	// INFO: обновление токена если он протух
	if expTime, err := strconv.ParseInt(value[0], 10, 64); err == nil {
		if expTime < time.Now().Unix() {
			var id int
			var login string
			for id, login = range value {
				if id != 0 {
					break
				}
			}
			err = SetCookieHandler(w, id, login)
			if err != nil {
				return nil, err
			}
		}
	} else {
		return nil, err
	}
	return value, nil
}

// SetCookieHandler утановка кук
func SetCookieHandler(w http.ResponseWriter, id int, login string) error {
	// INFO: размерность ключа 16,24,32
	var hashKey = []byte("0Lh|v|8WKCXapCpJQbTn}~Nb4o1j{zNC") // TODO: вынести в конфиг этот ключ
	var blockKey = []byte("F}arxw1nJJyPqFG%s3~u*YlUfNZQ@G85")
	var s = securecookie.New(hashKey, blockKey)
	expire := time.Now().Add(30 * time.Minute)
	value := map[int]string{
		0:  strconv.FormatInt(expire.Unix(), 10),
		id: login,
	}
	encoded, err := s.Encode("session", value)
	if err != nil {
		return err
	}
	cookie := &http.Cookie{
		Name:     "session",
		Value:    encoded,
		Path:     "/",
		HttpOnly: true,
		Expires:  expire,
		MaxAge:   90000, // TODO: прверить нужен ли этот параметр
	}

	http.SetCookie(w, cookie)
	// записываем токен в хранилище
	ls[encoded] = struct{}{}
	fmt.Println("session", encoded) // INFO: вывод кук для тестов
	return nil
}
