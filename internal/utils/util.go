package utils

import "strings"

// StrToLowerTrimSpace обрезаем пробелы и переводим в нихний регистр
func StrToLowerTrimSpace(in string) string {
	in = strings.ToLower(in)
	in = strings.TrimSpace(in)
	return in
}
