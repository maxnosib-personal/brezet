package utils

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"path"
	"path/filepath"

	yaml "gopkg.in/yaml.v2"
)

var (
	errNotAllowedFileExtension = errors.New("config file extension is not allowed")
	errInvalidFileExtension    = errors.New("invalid config file extension")
)

// LoadConfig parsing config
func LoadConfig(name string) (*Config, error) {
	var cfg Config
	if err := parseFile(name, &cfg); err != nil {
		return nil, fmt.Errorf("config loading from file error: %s", err)
	}

	if err := cfg.validate(); err != nil {
		return nil, fmt.Errorf("config validation error: %s", err)
	}
	return &cfg, nil
}

// Validate config validation
func (cfg *Config) validate() error {
	// TODO: сделать проверки
	if cfg.App.Port == "" {
		return errors.New("Invalid port: " + cfg.App.Port)
	}
	return nil
}

// PrintConfig debug for config output that is in config will work if run with flag -p ex: go run main.go -p config.yaml
func PrintConfig(pathConf string) error {
	filename, err := filepath.Abs(pathConf)
	if err != nil {
		return err
	}
	yamlFile, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	var configData Config
	err = yaml.Unmarshal(yamlFile, &configData)
	if err != nil {
		return err
	}
	fmt.Printf("Value: %+v\n", configData)
	return nil
}

/*********************************** helper ***********************************/

// getExt check isset file
func getExt(name string) (string, error) {
	ext := path.Ext(name)
	if ext == "" {
		return "", errInvalidFileExtension
	}

	// not empty therefore at least the dot delimiter exists
	if ext[1:] == "" {
		return "", errInvalidFileExtension
	}

	return ext[1:], nil
}

// parseFile parses config file and fill up cfg
func parseFile(fileName string, cfg interface{}) error {
	ext, err := getExt(fileName)
	if err != nil {
		return err
	}

	allowedConfigExtensions := map[string]struct{}{FileTypeYaml: {}, FileTypeJSON: {}, FileTypeYml: {}}
	if _, ok := allowedConfigExtensions[ext]; !ok {
		return errNotAllowedFileExtension
	}

	configFile, err := ioutil.ReadFile(fileName)
	if err != nil {
		return err
	}

	switch ext {
	case FileTypeYaml, FileTypeYml:
		if err := yaml.Unmarshal(configFile, cfg); err != nil {
			return fmt.Errorf("yaml unmarshal error: %s", err)
		}
	case FileTypeJSON:
		if err := json.Unmarshal(configFile, cfg); err != nil {
			return fmt.Errorf("json unmarshal error: %s", err)
		}
	}

	return nil
}
