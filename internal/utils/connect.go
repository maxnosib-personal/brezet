package utils

import (
	"database/sql"
	"fmt"
)

// ConnectToDB create connect to db
func ConnectToDB(cfg *Config) (*sql.DB, error) {
	var err error
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		cfg.Connections.Pgsql.Host,
		cfg.Connections.Pgsql.Port,
		cfg.Connections.Pgsql.User,
		cfg.Connections.Pgsql.Password,
		cfg.Connections.Pgsql.DBname)

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		return nil, fmt.Errorf("err connect to DB: %w", err)
	}

	err = db.Ping()
	if err != nil {
		return nil, fmt.Errorf("err ping DB:  %w", err)
	}

	fmt.Println("Successfully connected!")
	return db, nil
}
