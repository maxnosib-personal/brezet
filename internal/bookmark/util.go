package bookmark

import (
	"strconv"
	"strings"
)

func sliceIntToStringFromSQL(in []int) string {
	res := "{"
	for _, val := range in {
		res += strconv.Itoa(val) + ","
	}
	res = strings.TrimSuffix(res, ",")
	res += "}"
	return res
}
