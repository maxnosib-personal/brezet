package bookmark

import (
	"strconv"
)

func (h *Handler) insert(in *Model) error {
	err := h.db.QueryRow(`INSERT INTO bookmarks 
						(url,category_id,title,description,image,hashtag_ids,user_id,created_at,updated_at) 
						VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9) RETURNING id`,
		in.URL, in.CategoryID, in.Title, in.Description, in.Image,
		sliceIntToStringFromSQL(in.HashtagIds), in.UserID, in.CreatedAt, in.UpdatedAt).
		Scan(&in.ID)
	return err
}

func (h *Handler) get(url string, categoryID, userID int) ([]Model, error) {
	// TODO: везде сделать инициализацию чтоб в ответе не был null
	var res []Model
	query := "SELECT id,url,category_id,title,description,image,hashtag_ids,user_id,created_at,updated_at FROM bookmarks WHERE user_id=$1"
	if url != "" {
		query += " AND url LIKE'%" + url + "%'"
	}
	if categoryID != 0 {
		str := strconv.Itoa(categoryID)
		query += " AND category_id =" + str
	}
	rows, err := h.db.Query(query, userID)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var row Model
		var hID []uint8
		err = rows.Scan(&row.ID, &row.URL, &row.CategoryID, &row.Title, &row.Description, &row.Image, &hID,
			&row.UserID, &row.CreatedAt, &row.UpdatedAt)
		if err != nil {
			h.logger.Errorf("select #1: %v", err)
			continue
		}
		// массив в postgres храниться в виде строки которая приходить в виде []uint8 преобразуем ее в rune
		// и если это не цифра пропускаем
		row.HashtagIds = make([]int, 0, len(hID))
		for _, v := range hID {
			val, err := strconv.Atoi(string(v))
			if err != nil {
				continue
			}
			row.HashtagIds = append(row.HashtagIds, val)
		}

		res = append(res, row)
	}
	return res, err
}

func (h *Handler) update(in *Model) error {
	_, err := h.db.Exec(`UPDATE bookmarks SET 
						category_id=$1,title=$2,description=$3,image=$4,hashtag_ids=$5,updated_at=$6 WHERE id=$7 AND user_id=$8`,
		in.CategoryID, in.Title, in.Description, in.Image, sliceIntToStringFromSQL(in.HashtagIds), in.UpdatedAt, in.ID, in.UserID)
	return err
}

func (h *Handler) delete(id, uID int) error {
	_, err := h.db.Exec(`DELETE FROM bookmarks where id=$1 AND user_id=$2`, id, uID)
	return err
}
