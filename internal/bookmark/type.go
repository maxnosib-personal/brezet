package bookmark

import (
	"database/sql"
	"errors"

	"github.com/sirupsen/logrus"
)

// Handler package type
type Handler struct {
	db     *sql.DB
	logger *logrus.Logger
}

// Model user
type Model struct {
	ID          int    `json:"id"`
	CategoryID  int    `json:"categoryId"`
	UserID      int    `json:"userId"`
	URL         string `json:"url"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Image       string `json:"image"`
	HashtagIds  []int  `json:"hashtagIds"`
	CreatedAt   int64  `json:"createdAt"`
	UpdatedAt   int64  `json:"updatedAt"`
}

// New создание Handler
func New(db *sql.DB, logger *logrus.Logger) *Handler {
	return &Handler{db: db, logger: logger}
}

func (m *Model) validate() error {
	// TODO: добавить проверку что существует такой url для того чтоб не закончились id
	if m.URL == "" {
		return errors.New("url is empty")
	}
	if m.HashtagIds == nil {
		m.HashtagIds = []int{}
	}
	if m.CategoryID == 0 {
		m.CategoryID = 1
	}
	return nil
}
