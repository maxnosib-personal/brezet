package route

import (
	"database/sql"
	"net/http"

	"gitlab.com/maxnosib-personal/brezet.git/internal/bookmark"
	"gitlab.com/maxnosib-personal/brezet.git/internal/category"
	"gitlab.com/maxnosib-personal/brezet.git/internal/hashtag"
	"gitlab.com/maxnosib-personal/brezet.git/internal/user"
	"gitlab.com/maxnosib-personal/brezet.git/internal/utils"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

// InitRoute list routes
func InitRoute(r *mux.Router, db *sql.DB, logger *logrus.Logger, cfg *utils.Config) {
	api := r.PathPrefix("/api").Subrouter()
	// group user
	userS := user.New(db, logger)
	userG := api.PathPrefix("/user").Subrouter()
	userG.Use(CheckCookie)
	userG.HandleFunc("", userS.Create).Methods(http.MethodPost)
	userG.HandleFunc("", userS.Get).Methods(http.MethodGet)
	userG.HandleFunc("/{id:[0-9]+}", userS.Update).Methods(http.MethodPut)
	userG.HandleFunc("/{id:[0-9]+}", userS.Delete).Methods(http.MethodDelete)
	api.HandleFunc("/login", userS.Login).Methods(http.MethodPost)
	// group bookmark
	bookmarkS := bookmark.New(db, logger)
	bookmarkG := api.PathPrefix("/bookmark").Subrouter()
	bookmarkG.Use(CheckCookie)
	bookmarkG.HandleFunc("", bookmarkS.Create).Methods(http.MethodPost)
	bookmarkG.HandleFunc("", bookmarkS.Get).Methods(http.MethodGet)
	bookmarkG.HandleFunc("/{id:[0-9]+}", bookmarkS.Update).Methods(http.MethodPut)
	bookmarkG.HandleFunc("/{id:[0-9]+}", bookmarkS.Delete).Methods(http.MethodDelete)
	// group category
	categoryS := category.New(db, logger)
	categoryG := api.PathPrefix("/category").Subrouter()
	categoryG.Use(CheckCookie)
	categoryG.HandleFunc("", categoryS.Create).Methods(http.MethodPost)
	categoryG.HandleFunc("", categoryS.Get).Methods(http.MethodGet)
	categoryG.HandleFunc("/{id:[0-9]+}", categoryS.Update).Methods(http.MethodPut)
	categoryG.HandleFunc("/{id:[0-9]+}", categoryS.Delete).Methods(http.MethodDelete)
	// group hashtag
	hashtagS := hashtag.New(db, logger)
	hashtagG := api.PathPrefix("/hashtag").Subrouter()
	hashtagG.Use(CheckCookie)
	hashtagG.HandleFunc("", hashtagS.Create).Methods(http.MethodPost)
	hashtagG.HandleFunc("", hashtagS.Get).Methods(http.MethodGet)
	hashtagG.HandleFunc("/{id:[0-9]+}", hashtagS.Delete).Methods(http.MethodDelete)
}
