package route

import (
	"net/http"
	"strconv"

	"gitlab.com/maxnosib-personal/brezet.git/internal/utils"
)

// CheckCookie проверка кук
func CheckCookie(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cokieVal, err := utils.ReadCookieHandler(w, r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
			return
		}
		// cokieVal = map[int]string{1: "admin"}
		for id, login := range cokieVal {
			r.Header.Set("userID", strconv.Itoa(id))
			r.Header.Set("userLogin", login)
		}
		h.ServeHTTP(w, r)
	})
}
