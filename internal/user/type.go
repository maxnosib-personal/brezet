package user

import (
	"database/sql"
	"errors"

	"github.com/sirupsen/logrus"
)

// Handler package type
type Handler struct {
	db     *sql.DB
	logger *logrus.Logger
}

// Model user
type Model struct {
	ID    int    `json:"id"`
	Login string `json:"login"`
	Pwd   string `json:"password"` // TODO: сделать пароль не обязательным при возвращении
}

// Login user
type Login struct {
	Login string `json:"login"`
	Pwd   string `json:"password"`
}

// New создание Handler
func New(db *sql.DB, logger *logrus.Logger) *Handler {
	return &Handler{db: db, logger: logger}
}

func (m *Model) validate() error {
	if m.Login == "" {
		return errors.New("login is empty")
	}
	if m.Pwd == "" {
		return errors.New("password is empty")
	}
	return nil
}

func (m *Login) validate() error {
	if m.Login == "" {
		return errors.New("login is empty")
	}
	if m.Pwd == "" {
		return errors.New("password is empty")
	}
	return nil
}
