package user

func (h *Handler) isExistUser(login string) (bool, error) {
	model, err := h.get(login)
	if err != nil {
		return false, err
	}
	if len(model) != 0 {
		return false, err
	}
	return true, nil
}
