package user

func (h *Handler) insert(in *Model) error {
	err := h.db.QueryRow(`INSERT INTO users (login, password) VALUES ($1, $2) RETURNING id`,
		in.Login, in.Pwd).Scan(&in.ID)
	return err
}

func (h *Handler) get(login string) ([]Model, error) {
	var res []Model
	query := "SELECT id, login FROM users"
	if login != "" {
		query += " WHERE login LIKE'%" + login + "%'" //nolint: gosec, gocritic
	}

	rows, err := h.db.Query(query)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var row Model
		err = rows.Scan(&row.ID, &row.Login)
		if err != nil {
			h.logger.Errorf("select #1: %v", err)
			continue
		}

		res = append(res, row)
	}
	return res, err
}

func (h *Handler) update(in *Model) error {
	_, err := h.db.Exec(`UPDATE users SET login=$1, password=$2 WHERE id=$3`, in.Login, in.Pwd, in.ID)
	return err
}

func (h *Handler) delete(id int) error {
	_, err := h.db.Exec(`DELETE FROM users where id=$1`, id)
	return err
}

func (h *Handler) getByLogin(login string) (Model, error) {
	var res Model
	err := h.db.QueryRow(`SELECT id, login, password FROM users WHERE login=$1`, login).
		Scan(&res.ID, &res.Login, &res.Pwd)
	return res, err
}
