package user

import (
	"errors"
	"net/http"
	"strconv"

	"gitlab.com/maxnosib-personal/brezet.git/internal/utils"

	"github.com/gorilla/mux"
	"golang.org/x/crypto/bcrypt"
)

// Create регистрация пользователя
func (h *Handler) Create(w http.ResponseWriter, r *http.Request) {
	var res utils.Answer
	var hash []byte
	defer func() {
		utils.WriteJSON(w, res)
	}()
	in := new(Model)
	utils.ReadJSON(w, r, in)
	res.Err = in.validate()
	if res.Err != nil {
		return
	}
	// проверяем есть ли такой пользователь в системе
	if ok, err := h.isExistUser(in.Login); !ok || err != nil {
		res.Err = errors.New("user exist")
		return
	}

	// делаем хеш от пароля
	hash, res.Err = bcrypt.GenerateFromPassword([]byte(in.Pwd), 0)
	if res.Err != nil {
		return
	}
	in.Pwd = string(hash)
	res.Err = h.insert(in)
	if res.Err != nil {
		return
	}
	res.Data = in
}

// Get получение списка пользователей
func (h *Handler) Get(w http.ResponseWriter, r *http.Request) {
	var res utils.Answer
	defer func() {
		utils.WriteJSON(w, res)
	}()
	login := r.FormValue("login")
	res.Data, res.Err = h.get(login)
}

// Update обновление пользователя
func (h *Handler) Update(w http.ResponseWriter, r *http.Request) {
	var res utils.Answer
	var hash []byte
	defer func() {
		utils.WriteJSON(w, res)
	}()

	in := new(Model)
	utils.ReadJSON(w, r, in)
	in.ID, res.Err = strconv.Atoi(mux.Vars(r)["id"])
	if res.Err != nil {
		return
	}
	res.Err = in.validate()
	if res.Err != nil {
		return
	}
	// делаем хеш от пароля
	hash, res.Err = bcrypt.GenerateFromPassword([]byte(in.Pwd), 0)
	if res.Err != nil {
		return
	}
	in.Pwd = string(hash)
	res.Err = h.update(in)
	if res.Err != nil {
		return
	}
	res.Data = in
}

// Delete удаление пользователя
func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {
	var res utils.Answer
	defer func() {
		utils.WriteJSON(w, res)
	}()
	var id int
	id, res.Err = strconv.Atoi(mux.Vars(r)["id"])
	if res.Err != nil {
		return
	}
	res.Err = h.delete(id)
	if res.Err != nil {
		return
	}
	res.Data = true
}

// Login удаление пользователя
func (h *Handler) Login(w http.ResponseWriter, r *http.Request) {
	var res utils.Answer
	var user Model
	defer func() {
		utils.WriteJSON(w, res)
	}()
	in := new(Login)
	utils.ReadJSON(w, r, in)
	res.Err = in.validate()
	if res.Err != nil {
		return
	}
	user, res.Err = h.getByLogin(in.Login)
	// сравниваем пароль из бд с тем то прислал пользователь
	res.Err = bcrypt.CompareHashAndPassword([]byte(user.Pwd), []byte(in.Pwd))
	if res.Err != nil {
		return
	}

	res.Data = true
	// устанавливаем куки
	res.Err = utils.SetCookieHandler(w, user.ID, in.Login)
}
