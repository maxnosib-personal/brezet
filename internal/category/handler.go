package category

import (
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/maxnosib-personal/brezet.git/internal/utils"
)

// Create добавление ссылки
func (h *Handler) Create(w http.ResponseWriter, r *http.Request) {
	var res utils.Answer
	var uID int
	defer func() {
		utils.WriteJSON(w, res)
	}()
	uID, res.Err = strconv.Atoi(r.Header.Get("userID"))
	if res.Err != nil {
		return
	}
	in := new(Model)
	in.CreatedAt = time.Now().Unix()
	in.UpdatedAt = time.Now().Unix()
	in.UserID = uID
	utils.ReadJSON(w, r, in)
	// уменьшение дублирования
	in.Name = utils.StrToLowerTrimSpace(in.Name)
	res.Err = in.validate()
	if res.Err != nil {
		return
	}
	res.Err = h.insert(in)
	if res.Err != nil {
		return
	}
	res.Data = in
}

// Get получение списка ссылок
func (h *Handler) Get(w http.ResponseWriter, r *http.Request) {
	var res utils.Answer
	var uID, parentID int
	defer func() {
		utils.WriteJSON(w, res)
	}()
	name := r.FormValue("name")
	parentID, res.Err = strconv.Atoi(r.FormValue("parentID"))
	uID, res.Err = strconv.Atoi(r.Header.Get("userID"))
	if res.Err != nil {
		return
	}
	res.Data, res.Err = h.get(name, parentID, uID)
}

// Update обновление ссылки
func (h *Handler) Update(w http.ResponseWriter, r *http.Request) {
	var res utils.Answer
	defer func() {
		utils.WriteJSON(w, res)
	}()

	in := new(Model)
	utils.ReadJSON(w, r, in)
	in.ID, res.Err = strconv.Atoi(mux.Vars(r)["id"])
	if res.Err != nil {
		return
	}
	in.UserID, res.Err = strconv.Atoi(r.Header.Get("userID"))
	if res.Err != nil {
		return
	}
	in.UpdatedAt = time.Now().Unix()
	res.Err = in.validate()
	if res.Err != nil {
		return
	}
	res.Err = h.update(in)
	if res.Err != nil {
		return
	}
	res.Data = in
}

// Delete удаление ссылки
func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {
	var res utils.Answer
	var id, uID int
	defer func() {
		utils.WriteJSON(w, res)
	}()
	uID, res.Err = strconv.Atoi(r.Header.Get("userID"))
	if res.Err != nil {
		return
	}
	id, res.Err = strconv.Atoi(mux.Vars(r)["id"])
	if res.Err != nil {
		return
	}
	res.Err = h.delete(id, uID)
	if res.Err != nil {
		return
	}
	res.Data = true
}
