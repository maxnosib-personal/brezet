package category

import (
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

var entry = logrus.NewEntry(&logrus.Logger{
	Formatter: &logrus.JSONFormatter{},
	Level:     logrus.InfoLevel,
	Out:       os.Stderr,
}).WithField("Test", "category")

func getRR(method string, request string) (*httptest.ResponseRecorder, *http.Request) {
	var body io.Reader
	if request != "" {
		body = strings.NewReader(request)
	}
	req := httptest.NewRequest(method, "/", body)
	rec := httptest.NewRecorder()
	return rec, req
}

func setHeaders(req *http.Request, headers map[string]string) {
	for k, v := range headers {
		req.Header.Set(k, v)
	}
}

type createCase struct {
	err        error
	method     string
	statusCode int
	request    string
	headers    map[string]string
}

var createCases = []createCase{
	{err: nil, method: http.MethodPost, statusCode: http.StatusOK, request: `{"id":1,"name":"categoryName","icon":"iconUrl","parentId":1}`,
		headers: map[string]string{"Content-Type": "application/json; charset=utf-8", "userID": "1", "userLogin": "testUser"}},
}

func TestCreate(t *testing.T) {
	for _, cas := range createCases {
		rec, req := getRR(cas.method, cas.request)
		setHeaders(req, cas.headers)

		db, mock, err := sqlmock.New()
		if err != nil {
			entry.Logger.Errorf("sqlmock.New err: %v\n", err)
		}
		rows := sqlmock.NewRows([]string{"id"}).AddRow(2)
		mock.ExpectQuery("INSERT INTO (.+) ").WillReturnRows(rows)

		handler := New(db, entry.Logger)
		handler.Create(rec, req)

		if cas.statusCode != rec.Code {
			entry.Logger.Errorf("handler.Create err: %v\n", err)
		}
	}
}

type getCase struct {
	err        error
	method     string
	statusCode int
	request    string
	headers    map[string]string
}

var getCases = []getCase{
	{err: nil, method: http.MethodGet, statusCode: http.StatusOK, request: "",
		headers: map[string]string{"Content-Type": "application/json; charset=utf-8", "userID": "1", "userLogin": "testUser"}},
}

func TestGet(t *testing.T) {
	for _, cas := range getCases {
		rec, req := getRR(cas.method, cas.request)
		setHeaders(req, cas.headers)

		db, mock, err := sqlmock.New()
		if err != nil {
			entry.Logger.Errorf("sqlmock.New err: %v\n", err)
		}
		rows := sqlmock.NewRows([]string{"id", "user_id", "created_at", "updated_at", "name", "icon", "parent_id"}).
			AddRow(1, 1, 1592696229, 1592696229, "name", "icon", 0)
		mock.ExpectQuery("SELECT (.+) FROM categories WHERE (.+)").WillReturnRows(rows)

		handler := New(db, entry.Logger)
		handler.Get(rec, req)

		if cas.statusCode != rec.Code {
			entry.Logger.Errorf("handler.Get err: %v\n", err)
		}
	}
}

type updateCase struct {
	err        error
	method     string
	statusCode int
	request    string
	headers    map[string]string
}

var updateCases = []updateCase{
	{err: nil, method: http.MethodPut, statusCode: http.StatusOK, request: `{"id":1,"name":"categoryName","icon":"iconUrl","parentId":1}`,
		headers: map[string]string{"Content-Type": "application/json; charset=utf-8", "userID": "1", "userLogin": "testUser"}},
}

func TestUpdate(t *testing.T) {
	for _, cas := range updateCases {
		rec, req := getRR(cas.method, cas.request)
		setHeaders(req, cas.headers)

		vars := map[string]string{
			"id": "1",
		}
		req = mux.SetURLVars(req, vars)

		db, mock, err := sqlmock.New()
		if err != nil {
			entry.Logger.Errorf("sqlmock.New err: %v\n", err)
		}
		mock.ExpectExec("UPDATE (.+) WHERE (.+)").WillReturnResult(sqlmock.NewResult(1, 1))

		handler := New(db, entry.Logger)
		handler.Update(rec, req)

		if cas.statusCode != rec.Code {
			entry.Logger.Errorf("handler.Get err: %v\n", err)
		}
	}
}

type deleteCase struct {
	err        error
	method     string
	statusCode int
	request    string
	headers    map[string]string
}

var deleteCases = []deleteCase{
	{err: nil, method: http.MethodDelete, statusCode: http.StatusOK, request: "",
		headers: map[string]string{"Content-Type": "application/json; charset=utf-8", "userID": "1", "userLogin": "testUser"}},
}

func TestDelete(t *testing.T) {
	for _, cas := range deleteCases {
		rec, req := getRR(cas.method, cas.request)
		setHeaders(req, cas.headers)

		vars := map[string]string{
			"id": "1",
		}
		req = mux.SetURLVars(req, vars)

		db, mock, err := sqlmock.New()
		if err != nil {
			entry.Logger.Errorf("sqlmock.New err: %v\n", err)
		}
		mock.ExpectExec("DELETE FROM (.+) WHERE (.+)").WillReturnResult(sqlmock.NewResult(1, 1))

		handler := New(db, entry.Logger)
		handler.Delete(rec, req)

		if cas.statusCode != rec.Code {
			entry.Logger.Errorf("handler.Get err: %v\n", err)
		}
	}
}
