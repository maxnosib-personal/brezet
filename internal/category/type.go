package category

import (
	"database/sql"
	"errors"

	"github.com/sirupsen/logrus"
)

// Handler package type
type Handler struct {
	db     *sql.DB
	logger *logrus.Logger
}

// Model user
type Model struct {
	ID        int    `json:"id"`
	UserID    int    `json:"userId"`
	Name      string `json:"name"`
	Icon      string `json:"icon"`
	ParentID  *int   `json:"parentId"`
	CreatedAt int64  `json:"createdAt"`
	UpdatedAt int64  `json:"updatedAt"`
}

// New создание Handler
func New(db *sql.DB, logger *logrus.Logger) *Handler {
	return &Handler{db: db, logger: logger}
}

func (m *Model) validate() error {
	if m.Name == "" {
		return errors.New("name is empty")
	}
	if m.Icon == "" {
		return errors.New("icon is empty")
	}
	return nil
}
