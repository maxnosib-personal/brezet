package category

import "strconv"

func (h *Handler) insert(in *Model) error {
	err := h.db.QueryRow(`INSERT INTO categories (user_id,created_at,updated_at,name,icon,parent_id) 
	VALUES ($1,$2,$3,$4,$5,$6) RETURNING id`,
		in.UserID, in.CreatedAt, in.UpdatedAt, in.Name, in.Icon, in.ParentID).Scan(&in.ID)
	return err
}

func (h *Handler) get(name string, parentID, userID int) ([]Model, error) {
	var res []Model
	query := "SELECT id,user_id,created_at,updated_at,name,icon,parent_id FROM categories WHERE user_id=$1"
	if name != "" {
		query += " AND name LIKE'%" + name + "%'"
	}
	if parentID != 0 {
		str := strconv.Itoa(parentID)
		query += " AND parent_id =" + str
	}
	rows, err := h.db.Query(query, userID)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var row Model
		err = rows.Scan(&row.ID, &row.UserID, &row.CreatedAt, &row.UpdatedAt, &row.Name, &row.Icon, &row.ParentID)
		if err != nil {
			h.logger.Errorf("select #1: %v", err)
			continue
		}

		res = append(res, row)
	}
	return res, err
}

func (h *Handler) update(in *Model) error {
	_, err := h.db.Exec(`UPDATE categories SET name=$1,icon=$2,parent_id=$3,updated_at=$4 WHERE id=$5 AND user_id=$6`,
		in.Name, in.Icon, in.ParentID, in.UpdatedAt, in.ID, in.UserID)
	return err
}

func (h *Handler) delete(id, uID int) error {
	_, err := h.db.Exec(`DELETE FROM categories WHERE id=$1 AND user_id=$2`, id, uID)
	return err
}
