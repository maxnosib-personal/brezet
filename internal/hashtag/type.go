package hashtag

import (
	"database/sql"
	"errors"

	"github.com/sirupsen/logrus"
)

// Handler package type
type Handler struct {
	db     *sql.DB
	logger *logrus.Logger
}

// Model user
type Model struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// New создание Handler
func New(db *sql.DB, logger *logrus.Logger) *Handler {
	return &Handler{db: db, logger: logger}
}

func (m *Model) validate() error {
	if m.Name == "" {
		return errors.New("name is empty")
	}
	return nil
}
