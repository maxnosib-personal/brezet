package hashtag

func (h *Handler) insert(in *Model) error {
	err := h.db.QueryRow(`INSERT INTO hashtags (name) VALUES ($1) RETURNING id`,
		in.Name).Scan(&in.ID)
	return err
}

func (h *Handler) get(name string) ([]Model, error) {
	var res []Model
	query := "SELECT id,name FROM hashtags"
	if name != "" {
		query += " WHERE name LIKE'%" + name + "%'"
	}

	rows, err := h.db.Query(query)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var row Model
		err = rows.Scan(&row.ID, &row.Name)
		if err != nil {
			h.logger.Errorf("select #1: %v", err)
			continue
		}

		res = append(res, row)
	}
	return res, err
}

func (h *Handler) delete(id int) error {
	_, err := h.db.Exec(`DELETE FROM hashtags WHERE id=$1`, id)
	return err
}

func (h *Handler) checkIsset(hashtagID int) (int, error) {
	var count int
	err := h.db.QueryRow(`SELECT count(id) FROM bookmarks WHERE array_position(hashtag_ids, $1)!=0`, hashtagID).Scan(&count)
	return count, err
}
