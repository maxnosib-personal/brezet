package hashtag

import (
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/maxnosib-personal/brezet.git/internal/utils"
)

// Create добавление хештега
func (h *Handler) Create(w http.ResponseWriter, r *http.Request) {
	var res utils.Answer
	defer func() {
		utils.WriteJSON(w, res)
	}()
	in := new(Model)
	utils.ReadJSON(w, r, in)
	// уменьшение дублирования
	in.Name = utils.StrToLowerTrimSpace(in.Name)
	res.Err = in.validate()
	if res.Err != nil {
		return
	}
	res.Err = h.insert(in)
	if res.Err != nil {
		return
	}
	res.Data = in
}

// Get получение списка хештегов
func (h *Handler) Get(w http.ResponseWriter, r *http.Request) {
	var res utils.Answer
	defer func() {
		utils.WriteJSON(w, res)
	}()
	name := r.FormValue("name")
	if res.Err != nil {
		return
	}
	res.Data, res.Err = h.get(name)
}

// Delete удаление хештега
func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {
	var res utils.Answer
	var id, count int
	defer func() {
		utils.WriteJSON(w, res)
	}()
	id, res.Err = strconv.Atoi(mux.Vars(r)["id"])
	if res.Err != nil {
		return
	}
	count, res.Err = h.checkIsset(id)
	if count != 0 || res.Err != nil {
		res.Data = true
		return
	}
	res.Err = h.delete(id)
	if res.Err != nil {
		return
	}
	res.Data = true
}
