package main

import (
	"log"
	"net/http"
	"os"
	"time"

	"gitlab.com/maxnosib-personal/brezet.git/internal/route"
	"gitlab.com/maxnosib-personal/brezet.git/internal/utils"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	goflag "github.com/jessevdk/go-flags"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
)

// ServiceName name of service.
const ServiceName = "Brezet API"

// ServiceVersion version.
var ServiceVersion = "0.0.0" // nolint

func main() {
	// создаем логгер
	var entry = logrus.NewEntry(&logrus.Logger{
		Formatter: &logrus.JSONFormatter{},
		Level:     logrus.InfoLevel,
		Out:       os.Stderr,
	}).WithField("service", ServiceName).
		WithField("version", ServiceVersion)

	// структура для флагов
	var flagData struct {
		Config string `short:"c" long:"config" default:"config.yaml" description:"Path to config file." required:"true"`
		Print  bool   `short:"p" long:"print" description:"Print config file and exit."`
	}
	// парсим флаги
	_, err := goflag.Parse(&flagData)
	if err != nil {
		entry.Errorf("parse flag error: %s", err)
		return
	}
	// читаем конфиг
	cfg, err := utils.LoadConfig(flagData.Config)
	if err != nil {
		entry.Errorf("load config error: %s", err)
		return
	}
	// создаем подключение к бд
	db, err := utils.ConnectToDB(cfg)
	if err != nil {
		entry.Errorf("connect db error: %s", err)
		return
	}
	r := mux.NewRouter()

	// устанавливаем заголовки
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS", "DELETE", "PATH", "OPTIONS"})
	headersOk := handlers.AllowedHeaders([]string{"Accept", "Content-Type", "Content-Length",
		"Accept-Encoding", "X-CSRF-Token", "Authorization", "X-Requested-With"})
	credentialsOk := handlers.AllowCredentials()
	route.InitRoute(r, db, entry.Logger, cfg)
	entry.Info("start server")

	srv := &http.Server{
		Handler:      handlers.CORS(headersOk, originsOk, methodsOk, credentialsOk)(r),
		Addr:         cfg.App.Port,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal("ListenAndServe: ", srv.ListenAndServe())
}
