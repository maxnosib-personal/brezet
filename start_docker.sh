#!/bin/sh

# если запустить с флагом -first - будем делать первоначальную настройку системы
if [ "$1" = "-first" ]; then
  echo "start first run"
  sh sh/first_run.sh
  echo "TT"
fi

# гасим кониейнеры 
docker-compose down
# убиваем процессы на всякий случай
# sudo kill $(sudo lsof -t -i:54320)
# sudo kill $(sudo lsof -t -i:8080)
# TODO: ------- убрать потом когда будет бд постоянна
# удаляем записи о миграциях 
rm data/migrations.db
# запускаем в фоне контейнер с бд
docker-compose up -d db
# ждем пока контейнер запуститься
sleep 5
# задаем переменую что подключиться без пароля
export PGPASSWORD=postgres
# подключаемся и создаем бд
docker-compose exec db createdb -U postgres arii
# применяем миграции
migrago -c config_migrations.yaml up -p brezet -d arii
# обновляем и запускаем nginx
docker-compose up --build -d webserver


