# основной образ их может быть несколько
FROM golang:1.14.4-stretch AS builder
# описание что это за образ и др метаданные
LABEL author="max"
LABEL version="1.0"
LABEL description=" это первый тестовый проект\
из серии давай сделаем это"
# создаём переменную окружения
ENV ADMIN="max"
# run установка чего нибудь не достающего run может быть несколько 
# создаем папку, где будет наша программа
RUN mkdir -p /app/

COPY ./ /app
# WORKDIR меняет текущую рабочую директорию в контейнере для инструкций: COPY, ADD, RUN и ENTRYPOINT.
# идем в папку
WORKDIR /app

# билдимся
RUN go mod vendor
RUN CGO_ENABLED=0 GOOS=linux go build -o main .
# объявляем конейнер для исполнения
FROM alpine:3.12 AS production
# копируем бинарник в новый контейнер
COPY --from=builder /app .
# пробрасываем порт программы наружу образа (порт доcтупер только из др контейнеров)
EXPOSE 8080
# ENTRYPOINT задаем дефолтные команды и аргументы во время запуска контейнера
ENTRYPOINT ["sh", "sh/update_and_run_server.sh"]






